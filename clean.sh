#!/bin/bash

# dzn-html-elements -- Dezyne components for html elements
# Copyright (C) 2019, 2020 Henk Katerberg <hank@mudball.nl>
# Copyright (C) 2019, 2020 Verum Software Tools B.V. <support@verum.com>
#
# This file is part of dzn-html-elements.
#
# dzn-html-elements is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dzn-html-elements is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

rm -f js/dzn/IAlert.js
rm -f js/dzn/IButton.js
rm -f js/dzn/IContainer.js
rm -f js/dzn/INumberInput.js
rm -f js/dzn/IParagraph.js
rm -f js/dzn/ITextInput.js
rm -f js/dzn/ITimer.js
rm -f js/dzn/IWebSocketAPI.js
rm -f js/dzn/IXMLHttpRequest.js

rm -f js/dzn/CountDown.js
rm -f js/dzn/DemoHttpGet.js
rm -f js/dzn/Types.js
rm -f js/dzn/WebStopcontact.js

rm -f js/dzn/runtime.js
