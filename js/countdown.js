function instantiate_dzn_top_level_component() {
    var runtime = new dzn.runtime (function() {console.error ('test: illegal'); throw new Error ('test: oops: illegal'); });
    var loc = new dzn.locator ().set (runtime);

    countdown = new dzn.CountDown(loc, {name: 'countdown'});
}

function initialize_dzn_top_level_component() {
    countdown.lc.in.connect();
}

// load dependencies and on success instantiate and initialize top-level Dezyne system
loadScript('js/dzn/runtime.js')
    .then(() => loadScript('js/dzn/CountDown.js')) // top-level Dezyne system: CountDown
    .then(() => {
        instantiate_dzn_top_level_component()
        initialize_dzn_top_level_component()
    })
    .catch((e) => console.error('Script load failed: ' + e.target.src))
