// dzn-html-elements -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-html-elements.
//
// dzn-html-elements is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-html-elements is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script src="js/dzn/Alert.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IAlert'));}


dzn.Alert = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl'];
  // this._dzn.flushes = true;





  this.ctrl = new dzn.IAlert({provides: {name: 'ctrl', component: this}, requires: {}});




  this.ctrl.in.alert = function(s){
    if (true) window.alert(s);
    else if ((!(true) && true)) this._dzn.rt.illegal();
    else this._dzn.rt.illegal();

    return;

  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: development
