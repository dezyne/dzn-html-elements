// dzn-html-elements -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-html-elements.
//
// dzn-html-elements is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-html-elements is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script src="js/dzn/WebSocketAPI.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IWebSocketAPI'));}


dzn.WebSocketAPI = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl'];
  // this._dzn.flushes = true;



  this.reply_bool = null;


  this.ctrl = new dzn.IWebSocketAPI({provides: {name: 'ctrl', component: this}, requires: {}});


  // Foreign code vvvvv
  this.websocket = null;
  this.openWebSocket = function(url) {
    try {
      console.log('OPEN: ' + url);
      this.websocket = new WebSocket(url);
      this.reply_bool = true;
      this.websocket.onopen = function(evt) {
        console.log('CONNECTED');
        this.ctrl.out.opened();
      }.bind(this);
      this.websocket.onclose = function(evt) {
        console.log('CLOSED');
        this.ctrl.out.closed();
      }.bind(this);
      this.websocket.onmessage = function(evt) {
        console.log('MESSAGE: '  + evt.data);
        this.ctrl.out.message(evt.data);
      }.bind(this);
      this.websocket.onerror = function(evt) {
        console.log('ERROR: ' + evt.data);
        this.ctrl.out.error(evt.data);
      }.bind(this);
    }
    catch(err) {
      console.log('OPEN Failed: ' + err.message);
      this.repy_bool = false;
    }
  };
  this.closeWebSocket = function() {
    console.log('CLOSING');
    this.websocket.close();
  }
  this.sendMessage = function(message) {
    try {
      this.websocket.send(message);
      this.reply_bool = true;
      console.log('SENT: ' + message);
    }
    catch(err) {
      console.log('SEND failed: ' + err.message);
      this.reply_bool = false;
    }
  }
  // Foreign code ^^^^^



  this.ctrl.in.open = function(url){
    if (true) this.openWebSocket(url);
    else if ((!(true) && true)) this._dzn.rt.illegal();
    else this._dzn.rt.illegal();

    return this.reply_bool;
  };
  this.ctrl.in.close = function(){
    if (true) this.closeWebSocket();
    else this._dzn.rt.illegal();

    return;
  };
  this.ctrl.in.send = function(message){
    if (true) this.sendMessage(message);
    else this._dzn.rt.illegal();

    return this.reply_bool;
  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: development
