#!/bin/bash

# dzn-html-elements -- Dezyne components for html elements
# Copyright (C) 2019, 2020 Henk Katerberg <hank@mudball.nl>
# Copyright (C) 2019, 2020 Verum Software Tools B.V. <support@verum.com>
#
# This file is part of dzn-html-elements.
#
# dzn-html-elements is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dzn-html-elements is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

dzn code -l javascript -I . -o js/dzn dzn/IAlert.dzn
dzn code -l javascript -I . -o js/dzn dzn/IButton.dzn
dzn code -l javascript -I . -o js/dzn dzn/IContainer.dzn
dzn code -l javascript -I . -o js/dzn dzn/INumberInput.dzn
dzn code -l javascript -I . -o js/dzn dzn/IParagraph.dzn
dzn code -l javascript -I . -o js/dzn dzn/ITextInput.dzn
dzn code -l javascript -I . -o js/dzn dzn/ITimer.dzn
dzn code -l javascript -I . -o js/dzn dzn/IWebSocketAPI.dzn
dzn code -l javascript -I . -o js/dzn dzn/IXMLHttpRequest.dzn

dzn code -l javascript -I . -o js/dzn dzn/CountDown.dzn
dzn code -l javascript -I . -o js/dzn dzn/DemoHttpGet.dzn
dzn code -l javascript -I . -o js/dzn dzn/Types.dzn
dzn code -l javascript -I . -o js/dzn dzn/WebStopcontact.dzn

dzn cat /share/runtime/javascript/dzn/runtime.js > js/dzn/runtime.js
