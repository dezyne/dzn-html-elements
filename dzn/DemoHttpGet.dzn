// dzn-html-elements -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-html-elements.
//
// dzn-html-elements is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-html-elements is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

import Alert.dzn;
import Button.dzn;
import HtmlDivElement.dzn;
import HtmlBodyElement.dzn;
import TextInput.dzn;
import XMLHttpRequest.dzn;

interface ILifeCycle
{
 in void connect();
 behaviour
 {
   on connect: {}
 }
}

component DemoHttpGet
{
  provides ILifeCycle lc;

  system
  {
    DemoHttpGetBhv bhv;
    XMLHttpRequest fetch;
    Button getButton;
    Button abortButton;
    Alert alert;
    TextInput url;

    HtmlDivElement div;
    HtmlBodyElement body;

    lc <=> bhv.lc;
    bhv.get <=> getButton.ctrl;
    bhv.abort <=> abortButton.ctrl;
    bhv.fetch <=> fetch.ctrl;
    bhv.alert <=> alert.ctrl;
    bhv.url <=> url.ctrl;

    div.ctrl <=> *;
    div.container <=> body.ctrl;
  }
}

component DemoHttpGetBhv
{
  provides ILifeCycle lc;
  requires IAlert alert;
  requires IButton get;
  requires IButton abort;
  requires ITextInput url;
  requires IXMLHttpRequest fetch;

  behaviour
  {
    bool initialized = false;
    [!initialized] on lc.connect(): {initialized = true; get.enable(); abort.enable(); url.enable();}
    [initialized] on lc.connect(): {}
    on get.clicked(): {DOMString addr; url.getValue(addr); get.disable(); fetch.get(addr);}
    on abort.clicked(): {get.disable(); abort.disable(); fetch.abort();}
    on fetch.loaded(txt): {get.enable(); alert.alert(txt);}
    on fetch.error(): {get.enable(); alert.alert($'XMLHttpRequest: Foute boel!'$);}
    on fetch.aborted(): {get.enable(); alert.alert($'XMLHttpRequest: Aborted.'$);}
  }
}
