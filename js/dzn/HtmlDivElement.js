// dzn-html-elements -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-html-elements.
//
// dzn-html-elements is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-html-elements is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script defer src="js/dzn/dzn-name-lookup.js"></script>
  <script src="js/dzn/HtmlDivElement.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IContainer'));}


dzn.HtmlDivElement = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl'];
  // this._dzn.flushes = true;





  this.ctrl = new dzn.IContainer({provides: {name: 'ctrl', component: this}, requires: {}});

  this.container = new dzn.IContainer({provides: {}, requires: {name: 'container', component: this}});


  // Foreign vvvvv
  this.htmlElement = document.createElement('div');
  this.htmlElement.name = shortName(this);
  this.htmlElement.id = fullName(this);

  // Add a paragraph with name of the Dezyne system which owns this div element.
  // On the GUI the div element is the box representing the scope of the system.
  var label = document.createElement('p');
  var parentName = this._dzn.meta.parent._dzn.meta.name;
  label.textContent = parentName;
  this.htmlElement.appendChild(label);
  
  // Nest inside higher level container. If no IContainer found, use
  // document.body as container.
  if (this.container._dzn.meta.provides.hasOwnProperty('component'))
    this.container._dzn.meta.provides.component.htmlElement.appendChild(this.htmlElement);
  else
    document.body.appendChild(this.htmlElement);
  // Foreign ^^^^^


  this.ctrl.in.SetTitle = function(text){
    if (true) ;
    else if ((!(true) && true)) this._dzn.rt.illegal();
    else this._dzn.rt.illegal();

    return;

  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: development
