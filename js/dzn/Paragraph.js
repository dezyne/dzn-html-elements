// dzn-html-elements -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-html-elements.
//
// dzn-html-elements is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-html-elements is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script defer src="js/dzn/dzn-name-lookup.js"></script>
  <script src="js/dzn/IParagraph.js"></script>
  <script src="js/dzn/Paragraph.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IParagraph'));}
if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IContainer'));}


dzn.Paragraph = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl', 'container'];
  // this._dzn.flushes = true;




  this.container = locator.get(new dzn.IContainer());

  this.ctrl = new dzn.IParagraph({provides: {name: 'ctrl', component: this}, requires: {}});


  // Foreign vvvvv
  this.divElement = document.createElement('div');
  this.labelElement = document.createElement('label');
  this.labelElement.textContent = this._dzn.meta.name;
  this.htmlElement = document.createElement('p');
  this.htmlElement.name = shortName(this);
  // Associate <label> with <intput> element:
  this.htmlElement.id = fullName(this);
  this.labelElement.for = this.htmlElement.id;
  // Put <label> and <input> into <div>
  this.divElement.appendChild(this.labelElement);
  this.divElement.appendChild(this.htmlElement);

  // Nest inside higher level container. If no IContainer found, use
  // document.body as container.
  if (this.container._dzn.meta.provides.hasOwnProperty('component'))
    this.container._dzn.meta.provides.component.htmlElement.appendChild(this.divElement);
  else
    document.body.appendChild(this.divElement);
  // Foreign ^^^^^


  this.ctrl.in.setLabel = function(text){
    if (true) this.labelElement.textContent = text;
    else this._dzn.rt.illegal();

    return;

  };
  this.ctrl.in.setInnerText = function(text){
    if (true) this.htmlElement.innerText = text;
    else this._dzn.rt.illegal();

    return;

  };
  this.ctrl.in.getInnerText = function(text){
    text.value = this.htmlElement.innerText;

    return;

  };
  this.ctrl.in.appendLine = function(text){
    if (true) this.htmlElement.innerText += text + "\n";
    else this._dzn.rt.illegal();

    return;

  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: 0.0.0
