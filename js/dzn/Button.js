// dzn-html-elements -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-html-elements.
//
// dzn-html-elements is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-html-elements is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-html-elements.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script defer src="js/dzn/dzn-name-lookup.js"></script>
  <script src="js/dzn/Button.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IButton'));}
if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IContainer'));}


dzn.Button = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl', 'container'];
//  this._dzn.flushes = true;




  this.container = locator.get(new dzn.IContainer());

  this.ctrl = new dzn.IButton({provides: {name: 'ctrl', component: this}, requires: {}});


  // Foreign vvvvv
  this.htmlElement = document.createElement('button');
  this.htmlElement.name = shortName(this);
  this.htmlElement.id = fullName(this);
  this.htmlElement.textContent = this._dzn.meta.name;
  this.htmlElement.addEventListener('click', function(evt) {
    console.log("CLICK!");
    this.ctrl.out.clicked();
  }.bind(this));

  // Nest inside higher level container. If no IContainer found, use
  // document.body as container.
  if (this.container._dzn.meta.provides.hasOwnProperty('component'))
    this.container._dzn.meta.provides.component.htmlElement.appendChild(this.htmlElement);
  else
    document.body.appendChild(this.htmlElement);
  // Foreign ^^^^^


  this.ctrl.in.enable = function(){
    if (true) this.htmlElement.disabled = false;
    else if ((!(true) && true)) this._dzn.rt.illegal();
    else this._dzn.rt.illegal();

    return;

  };
  this.ctrl.in.disable = function(){
    if (true) this.htmlElement.disabled = true;
    else if ((!(true) && true)) this._dzn.rt.illegal();
    else this._dzn.rt.illegal();

    return;

  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: development
