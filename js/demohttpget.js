function connect_html_page() {
  var runtime = new dzn.runtime (function() {console.error ('test: illegal'); throw new Error ('test: oops: illegal'); });
  var loc = new dzn.locator ().set (runtime);
  var demo = new dzn.DemoHttpGet(loc, {name: 'demohttpget'});
  demo.lc.in.connect();
}


// Page loads script with 'defer' =>  script executes before 'DOMContentLoaded event.
connect_html_page();
